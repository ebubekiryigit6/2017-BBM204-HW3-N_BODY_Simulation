javac ReadSimulateWrite.java 
java ReadSimulateWrite 25000000 50000 < inputs/planets.txt > output

%% For big universe with Color point representations
java ReadSimulateWrite 100 0.1 < inputs/input1.txt > output
