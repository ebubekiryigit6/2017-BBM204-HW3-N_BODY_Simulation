
/**
 * Created by Ebubekir.
 */
public class Node {

    Body body;              // node's body
    Universe universe;          // universe in 2D universe
    Node topLeftChild;      // node's top left child
    Node topRightChild;     // node's top right child
    Node bottomLeftChild;   // node's bottom left child
    Node bottomRightChild;  // node's bottom right child
    COM centerOfMass;       // COM includes center of mass and total mass of node's all bodies

    public static final int TOP_LEFT_CHILD = 1;
    public static final int TOP_RIGHT_CHILD = 2;
    public static final int BOTTOM_LEFT_CHILD = 3;
    public static final int BOTTOM_RIGHT_CHILD = 4;

    /**********************/
    /*         *          */
    /*    1    *     2    */
    /*         *          */
    /*         *          */    // this is my universe classification method
    /**********************/
    /*         *          */
    /*    3    *     4    */
    /*         *          */
    /**********************/




    public Node(Body body, Universe universe){
        this.body = body;
        this.universe = universe;
        this.centerOfMass = new COM(0,0,0);
    }



    /**
     * @return if node is a external node returns true, else returns false
     */
    public boolean isLeaf(){
        return topLeftChild == null && topRightChild == null && bottomLeftChild == null && bottomRightChild == null;
    }

    /**
     * @return if node is a internal node returns true, else returns false
     */
    public boolean isInternal(){
        return topLeftChild != null || topRightChild != null || bottomLeftChild != null || bottomRightChild != null;
    }



    /**
     * @return if node has a body returns true.
     */
    public boolean hasBody(){
        return body != null;
    }




    /**
     * inserts a body in Node(Universe)
     *
     * @param other planet to add
     * @return if successful insert operation returns true.
     */
    public boolean insert(Body other) {
        // it cant insert null body
        if (other == null) {
            return false;
        }
        // node's body is empty we can insert
        if (!hasBody()) {
            this.body = other;
            centerOfMass.updateCenterOfMass(other);  // update node's center of mass
            return true;
        }
        else {

            int whereOther = universe.findChildSquare(other);    // Finds which child the planet is in
            int whereThis = universe.findChildSquare(this.body);

            // I confused i cant return that place
            //if (whereOther == -1 || whereThis == -1){
            //    return false;
            //}

            // now we have body but we have no child
            // so create all children and update center of mass
            // insert two bodies in children
            if (isLeaf()) {
                this.topLeftChild = new Node(null, universe.getTopLeft());
                this.topRightChild = new Node(null, universe.getTopRight());
                this.bottomLeftChild = new Node(null, universe.getBottomLeft());
                this.bottomRightChild = new Node(null, universe.getBottomRight());

                // go in a child
                put(other, whereOther);
                put(this.body, whereThis);

                centerOfMass.updateCenterOfMass(other);

                // just update center of mass
                // send body to appropriate child
            } else if (isInternal()) {
                centerOfMass.updateCenterOfMass(other);
                put(other, whereOther);
            }
            return true;

        }

    }




    /**
     * Traverses all tree and calculates net force on body from other bodies.
     *
     * @param other   affected body
     * @param G       G constant
     * @param RATIO   distance Ratio
     * @return        if it adds force successfully returns true
     */
    public boolean addForce(Body other,double G, double RATIO) {
        // if center of mass values equals body's values
        if (other == null || isBodyEqualCOM(other, centerOfMass)) {
            return false;
        }

        if (hasBody()) {
            // we have a body but we have no child
            if (isLeaf()) {
                other.addForce(centerOfMass, G);
            }
            // we have body and children
            // calculate distance ratio
            // add forces recursive
            else if (isInternal()) {
                double r = universe.getEdgeLength();
                double d = Math.sqrt(Math.pow(other.getPositionX() - centerOfMass.getCenterX(), 2) + Math.pow(other.getPositionY() - centerOfMass.getCenterY(), 2));


                if (r / d > RATIO) {
                    // search children recursive
                    topLeftChild.addForce(other, G, RATIO);
                    topRightChild.addForce(other, G, RATIO);
                    bottomLeftChild.addForce(other, G, RATIO);
                    bottomRightChild.addForce(other, G, RATIO);
                }
                // this is too far
                else {
                    other.addForce(centerOfMass, G);

                }
            }
        }
        return true;
    }


    /**
     * Choose which house to place
     * @param other body
     * @param where sub universe code (1,2,3,4)
     */
    private void put(Body other, int where){
        switch (where){
            case TOP_LEFT_CHILD:
                topLeftChild.insert(other);
                break;
            case TOP_RIGHT_CHILD:
                topRightChild.insert(other);
                break;
            case BOTTOM_LEFT_CHILD:
                bottomLeftChild.insert(other);
                break;
            case BOTTOM_RIGHT_CHILD:
                bottomRightChild.insert(other);
                break;
        }
    }

    private boolean isBodyEqualCOM(Body other, COM com){
        return other.getPositionX() == com.getCenterX() &&
                other.getPositionY() == com.getCenterY() &&
                other.getMass() == com.getMass();
    }

    public static void main(String[] args){

        Body th = new Body(2,8,3,0,0,5,"adsdad");
        Node node = new Node(null,new Universe(0,0,20));
        node.insert(new Body(0,15,15,0,0,5,"saasf"));
        node.insert(new Body(1,7,7,0,0,10,"adas"));
        node.insert(th);
        node.insert(new Body(3,7,1,0,0,7,"qdada"));

        node.addForce(th,6.67E-11,0.5);

        /*****  Algorith is true i think :)  ******/
    }
}
