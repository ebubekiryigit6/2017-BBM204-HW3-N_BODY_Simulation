/**
 *
 * Author:      Ebubekir
 *
 * File:        NBody.java
 *
 * Purpose:     N-Body Simulation with  -> Quad Tree Algorithm   -> Brute Force Algorithm
 *
 *
 *              In the first part, we are expected to calculate a N-body simulation algorithm
 *              with Brute Force (N ^ 2) and write a simulation that continuously plots the new position values.
 *
 *
 *              In the second part, we will try to increase the working speed of the algorithm by creating the N-Body simulation algorithm
 *              Quad Tree (NlogN). We will avoid some unnecessary calculations
 *              by calculating the forces acting on each element on the Quad Tree.
 *
 *
 * Compile:     javac NBody.java
 *
 *
 * Run:         java NBody <brute or quad> <Total Time> <Time Change> < input.txt > output.txt
 *
 *
 * Input:       A text file with this format
 *
 *              2001          (number of bodies)
 *              2.83800E06    (universe radius)
 *              0.00000E00 0.00000E00 0.00000E00 0.00000E00 6.00000E26  0 0 255
 *              (Body) <position X> <position Y> <velocity X> <velocity Y> <mass> <color codes>
 *
 *
 *
 * Output:      The program outputs the values of each planet such as
 *              position, speed, mass in the last state.
 *
 *
 * Notes:       none
 *
 *
 *
 */


import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;


public class NBody {

    private static final String BRUTE_ALGORITHM = "brute";
    private static final String QUAD_ALGORITHM = "quad";

    public static void main(String[] args) throws IOException {

        ///****************************************   DELETE    *******************************************/
//
        System.setIn(new FileInputStream("inputs/pluto.txt"));    //    DELETE THIS BEFORE SUBMIT
        System.setOut(new PrintStream("output.txt"));             //    DELETE THIS BEFORE SUBMIT
//
        ///*****************************************  DELETE     ******************************************/


        double DELTA_T = Double.parseDouble(args[2]);   // time
        double TOTAL_T = Double.parseDouble(args[1]);   // end of time

        int N = Integer.parseInt(StdIn.readLine().trim().replaceAll("\\s+",""));                 // number of bodies
        double radius = Double.parseDouble(StdIn.readLine().trim().replaceAll("\\s+",""));       // radius of universe


        // simulate Brute Force Algorithm
        if (args[0].equalsIgnoreCase(BRUTE_ALGORITHM)){
            BruteForceAlgorithm bruteForceAlgorithm = new BruteForceAlgorithm(DELTA_T,TOTAL_T,N,radius);
            bruteForceAlgorithm.simulate();
        }
        // simulate Quad Tree Algorithm
        else if (args[0].equalsIgnoreCase(QUAD_ALGORITHM)){
            QuadAlgorithm quadAlgorithm = new QuadAlgorithm(DELTA_T,TOTAL_T,N,radius);
            quadAlgorithm.simulate();
        }
        // Wrong algorithm type!
        else {
            StdOut.println("Algorithm not found, Algorithm type can be \"quad\" or \"brute\" !");
            System.exit(1);
        }
    }

}
