/**
 * Created by Ebubekir.
 */
public class Universe {

    double centerX;
    double centerY;
    double radius;

    public Universe(double centerX, double centerY, double radius){
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
    }

    public boolean inUniverse(Body body){
        double x = body.getPositionX();
        double y = body.getPositionY();

        return x <= centerX + radius &&
                x >= centerX - radius &&
                y <= centerY + radius &&
                y >= centerY - radius;
    }

    public double getEdgeLength(){
        return radius * 2;
    }

    public Universe getTopLeft(){
        double newRadius = radius / 2.0;
        return new Universe(centerX - newRadius,centerY + newRadius,newRadius);
    }

    public Universe getTopRight(){
        double newRadius = radius / 2.0;
        return new Universe(centerX + newRadius, centerY + newRadius,newRadius);
    }

    public Universe getBottomLeft(){
        double newRadius = radius / 2.0;
        return new Universe(centerX - newRadius, centerY - newRadius, newRadius);
    }

    public Universe getBottomRight(){
        double newRadius = radius / 2.0;
        return new Universe(centerX + newRadius, centerY - newRadius, newRadius);
    }

    /**
     *
     * @param other body that is in control
     * @return returns place code of appropriate child
     */
    public int findChildSquare(Body other){
        if (this.getTopLeft().inUniverse(other)){
            return 1;
        }
        else if (this.getTopRight().inUniverse(other)){
            return 2;
        }
        else if (this.getBottomLeft().inUniverse(other)){
            return 3;
        }
        else if (this.getBottomRight().inUniverse(other)){
            return 4;
        }
        else {
            return -1;
        }
    }

}
