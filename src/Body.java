import java.awt.*;


/**
 * Created by Ebubekir.
 */

/**
 * Body object represents a planet.
 */
public class Body {

    int id;                                     // unique id
    private double netFx;                       // Net force under influence
    private double netFy;
    private double positionX;                   // position in universe
    private double positionY;
    private double velocityX;                   // velocity of planet
    private double velocityY;
    private double mass;                        // planet's mass
    private Color color;                        // drawing color
    private String imagePath;                   // drawing image path
    private boolean initWithImage = false;      // is body initialize with a image file?


    /**
     * Body constructor with Color codes
     *
     * @param id            id
     * @param positionX     position in x axis
     * @param positionY     position in y axis
     * @param velocityX     velocity in x axis
     * @param velocityY     velocity in y axis
     * @param mass          mass
     * @param color         sets pencil color (in drawing)
     */
    public Body(int id, double positionX, double positionY, double velocityX, double velocityY, double mass, Color color){
        init(id,positionX,positionY,velocityX,velocityY,mass);
        this.color = color;
        this.imagePath = null;
        // initWithImages already false
    }

    /**
     * Body constructor with image and image path
     *
     * @param id                id
     * @param positionX         position in x axis
     * @param positionY         position in y axis
     * @param velocityX         velocity in x axis
     * @param velocityY         velocity in y axis
     * @param mass              mass
     * @param imagePath         sets body's image (in drawing)
     */
    public Body(int id,double positionX, double positionY, double velocityX, double velocityY, double mass, String imagePath){
        init(id,positionX,positionY,velocityX,velocityY,mass);
        this.imagePath = imagePath;
        this.initWithImage = true;
        this.color = null;
    }

    /* initialize body */
    private void init(int id, double positionX, double positionY, double velocityX, double velocityY, double mass){
        this.id = id;
        this.netFx = 0;
        this.netFy = 0;
        this.positionX = positionX;
        this.positionY = positionY;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.mass = mass;
    }

    /**
     * This function updates position of body.
     * There is total net force from other planets.
     * The acceleration is found using the formula "F = m.a".
     * There is a new speed from the formula "V = V + a.t".
     * Finally, the new position is calculated from the formula "X = X + V.t" and the new position of the planet is updated.
     *
     * @param Fx        x axis of total net force from other planets
     * @param Fy        y axis of total net force from other planets
     * @param DELTA_T   time change
     */
    public void setNewPosition(double Fx, double Fy, double DELTA_T){
        double accelerationX = Fx / getMass();
        double accelerationY = Fy / getMass();    // F = m.a

        velocityX += accelerationX * DELTA_T;
        velocityY += accelerationY * DELTA_T;     // V = V + a.t

        positionX += velocityX * DELTA_T;
        positionY += velocityY * DELTA_T;         // X = X + V.t
    }

    /**
     * This function finds the force that a planet has on the other planet.
     *
     * @param other     // affects a force this planet
     * @param G         // G constant
     */
    public void addForce(Body other, double G){
        // F = ((m1 * m2) * G) / (r*r)
        double distance = Math.sqrt(Math.pow(positionX - other.getPositionX(),2) + Math.pow(positionY - other.getPositionY(),2));
        double scalarForce = (G * getMass() * other.getMass()) / Math.pow(distance,2);
        netFx += (scalarForce * (other.getPositionX() - positionX) / distance);
        netFy += (scalarForce * (other.getPositionY() - positionY) / distance);
    }

    /**
     * This function calculates the force applied by a mass center of mass planets to a planet.
     *
     * @param centerOfMass   // affects a force this planet
     * @param G              // G constant
     */
    public void addForce(COM centerOfMass, double G){
        // F = ((m1 * m2) * G) / (r*r)
        double distance = Math.sqrt(Math.pow(positionX - centerOfMass.getCenterX(),2) + Math.pow(positionY - centerOfMass.getCenterY(),2));
        double scalarForce = (G * getMass() * centerOfMass.getMass()) / Math.pow(distance,2);
        netFx += (scalarForce * (centerOfMass.getCenterX() - positionX) / distance);
        netFy += (scalarForce * (centerOfMass.getCenterY() - positionY) / distance);
    }

    /**
     * Resets planet's net force
     */
    public void clearForce(){
        netFx = 0;
        netFy = 0;
    }

    /**
     * Clones a body
     * @return cloned body
     */
    public Body cloneBody(){
        if (isInitWithImage()){
            return new Body(id,positionX,positionY,velocityX,velocityY,mass,imagePath);
        }
        else {
            return new Body(id,positionX,positionY,velocityX,velocityY,mass,color);
        }
    }

    public double getMass() {
        return mass;
    }

    public double getPositionX() {
        return positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    public double getVelocityX() {
        return velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public double getNetFx() {
        return netFx;
    }

    public double getNetFy() {
        return netFy;
    }

    public Color getColor() {
        return color;
    }

    public String getImagePath() {
        return imagePath;
    }

    public boolean isInitWithImage() {
        return initWithImage;
    }

    public int getId() {
        return id;
    }
}
