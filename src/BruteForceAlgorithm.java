import java.io.File;
import java.util.ArrayList;

/**
 * Created by Ebubekir.
 */

/**
 *  N-Body simulation brute force algorithm.
 *  Read initial values of planets from file and calculate
 *  and plot them according to brute force algorithm.
 */
public class BruteForceAlgorithm extends Simulation{

    private double DELTA_T;            // time change
    private double TOTAL_T;            // total time
    private int N;                     // number of body
    private double radius;             // universe radius
    private ArrayList<Body> bodies;    // reads bodies in a file


    public BruteForceAlgorithm(double DELTA_T, double TOTAL_T, int N, double radius){
        this.DELTA_T = DELTA_T;
        this.TOTAL_T = TOTAL_T;
        this.N = N;
        this.radius = radius;
        bodies = readBodies(N);
    }

    /**
     * It simulates planets according to the given algorithm.
     */
    @Override
    public void simulate(){

        // crate a universe with given radius ( a java swing frame )
        StdDraw.show(0);
        StdDraw.setXscale(-radius, +radius);
        StdDraw.setYscale(-radius, +radius);


        // Changes time every step
        for (double t = 0.0; t < TOTAL_T; t = t + DELTA_T) {

            // clear frame
            if (bodies.get(0).isInitWithImage()){
                StdDraw.clear();
                StdDraw.picture(0, 0,  "images/starfield.jpg");
            }
            else {
                StdDraw.clear(StdDraw.BLACK);
            }


            // calculate force on each body
            for (int i = 0; i < bodies.size(); i++){
                for (int j = 0; j < bodies.size(); j++){
                    if (j != i){
                        bodies.get(i).addForce(bodies.get(j),G);
                    }
                }
            }

            // calculate new position according to new net forces
            for (int i = 0; i < bodies.size(); i++){
                bodies.get(i).setNewPosition(bodies.get(i).getNetFx(),bodies.get(i).getNetFy(),DELTA_T);
            }

            // draw all bodies on frame
            for (int i = 0; i < bodies.size(); i++){
                draw(bodies.get(i));
            }

            // clear net forces for other steps
            for (int i = 0; i < bodies.size(); i++){
                bodies.get(i).clearForce();
            }
            // Sleeps threads to see drawing. Because program is too fast.
            StdDraw.show(SIMULATION_SPEED);
        }

        /*
            print all bodies after simulation finish.
         */


        StdOut.printf("%d\n", N);
        StdOut.printf("%.2e\n", radius);

        for (int i = 0; i < bodies.size(); i++){
            Body temp = bodies.get(i);
            if (temp.isInitWithImage()){
                StdOut.printf("%11.4e %11.4e %11.4e %11.4e %11.4e %s\n", temp.getPositionX(),
                        temp.getPositionY(),
                        temp.getVelocityX(),
                        temp.getVelocityY(),
                        temp.getMass(),
                        new File(temp.getImagePath()).getName());
            }
            else {
                StdOut.printf("%11.4e %11.4e %11.4e %11.4e %11.4e %d %d %d\n", temp.getPositionX(),
                        temp.getPositionY(),
                        temp.getVelocityX(),
                        temp.getVelocityY(),
                        temp.getMass(),
                        temp.getColor().getRed(),
                        temp.getColor().getGreen(),
                        temp.getColor().getBlue());
            }
        }
    }

}
