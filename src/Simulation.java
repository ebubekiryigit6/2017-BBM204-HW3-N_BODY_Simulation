import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Ebubekir.
 */

/**
 * It imposes the simulate () function on the simulation algorithms.
 * Holds certain variables. Creates drawing functions of planets.
 * According to the number of planets given Reads the planets from the File and stores them in an ArrayList.
 */
public abstract class Simulation {

    public double G = 6.67E-11;            // G constant
    public int SIMULATION_SPEED = 10;     // time of thread sleep
    public double RATIO = 0.5;             // distance ratio

    /**
     *  It simulates planets according to the given algorithm.
     */
    public abstract void simulate();

    /**
     *
     * @param body planet that we want to draw in simulation
     */
    protected void draw(Body body){
        // if body is initialize with image draw body with picture
        if (body.isInitWithImage()){
            StdDraw.picture(body.getPositionX(),body.getPositionY(),"images/" + body.getImagePath());
        }
        // if body is initialize with color codes, draw body with color codes
        else {
            StdDraw.setPenColor(body.getColor());
            //StdDraw.setPenRadius(0.010);
            StdDraw.point(body.getPositionX(),body.getPositionY());
        }
    }

    /**
     * Reads bodies in a text file.
     *
     * @param N     number of bodies
     * @return      returns a ArrayList that contains all bodies
     */
    protected ArrayList<Body> readBodies(int N){
        ArrayList<Body> bodies = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            String line = StdIn.readLine().trim();                // clear whitespaces
            String[] splitted = line.split("\\s+");          // split with any whitespaces or tab characters
            double positionX = Double.parseDouble(splitted[0]);
            double positionY = Double.parseDouble(splitted[1]);
            double velocityX = Double.parseDouble(splitted[2]);
            double velocityY = Double.parseDouble(splitted[3]);
            double mass = Double.parseDouble(splitted[4]);

            // inits with image path
            if (splitted.length == 6){
                String imagePath = splitted[5];
                bodies.add(new Body(i,positionX,positionY,velocityX,velocityY,mass,imagePath));
            }
            // inits with color codes
            else if (splitted.length == 8) {
                int red     = Integer.parseInt(splitted[5].trim().replaceAll("\\s+",""));
                int green   = Integer.parseInt(splitted[6].trim().replaceAll("\\s+",""));
                int blue    = Integer.parseInt(splitted[7].trim().replaceAll("\\s+",""));
                bodies.add(new Body(i,positionX,positionY,velocityX,velocityY,mass,new Color(red,green,blue)));
            }
            // this is not legal !!
            else {
                StdOut.println("Input format is wrong! Input format must be (px,py,vx,vy,m,picture or color values) !");
            }
        }
        return bodies;
    }
}
