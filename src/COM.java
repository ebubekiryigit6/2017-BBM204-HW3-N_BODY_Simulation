/**
 * Created by Ebubekir.
 */

/**
 *  Center of mass object
 */
public class COM {

    private double centerX;     // position on x-axis
    private double centerY;     // position on y-axis
    private double mass;        // total mass

    public COM(double centerX, double centerY, double mass){
        this.centerX = centerX;
        this.centerY = centerY;
        this.mass = mass;
    }

    /**
     * updates center of mass object with body object
     * x = (m1 * x1 + m2 * x2) / m
     * y = (m1 * y1 + m2 * y2) / m
     * m = m1 + m2
     *
     * @param other   body object
     */
    public void updateCenterOfMass(Body other){

        if (centerX == 0 && centerY == 0 && mass == 0){
            centerX = other.getPositionX();
            centerY = other.getPositionY();
            mass = other.getMass();
        }
        else {
            double totalMass = mass + other.getMass();
            double posX = (centerX * mass + other.getPositionX() * other.getMass()) / totalMass;
            double posY = (centerY * mass + other.getPositionY() * other.getMass()) / totalMass;

            centerX = posX;
            centerY = posY;
            mass = totalMass;
        }
    }


    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getMass() {
        return mass;
    }
}
